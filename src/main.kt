
import kotlin.math.sqrt

fun main() {

    val n1 = Point(1.0, 2.0)
    val n2 = Point(5.0, 10.0)


    // Point type


    println(n1.toString())
    println(n2.toString())
    println(n1.equals(n2))
    println(n1.movePoint())
    println(n2.movePoint())
    println(n1.calculateDistance(n2))


    // Fractions


    val f1 = Fraction(4.0, 8.0)
    val f2 = Fraction(10.0, 16.0)

    println(f1.reduction())
    println(f1.addition(f2))
    println(f1.subtraction(f2))
    println(f1.multiply(f2))
    println(f1.division(f2))

}

open class Point(var x: Double , var y: Double) {

    override fun toString(): String {
        return " $x , $y "
    }

    override fun equals(other: Any?): Boolean {
        if (other is Point){
            if(x == other.x && y == other.y)

                return true
        }
        return false
    }

    fun movePoint (): String {
        return "${-x} , ${-y}"
    }
    fun calculateDistance(other: Any?): Any {
        if (other is Point) {
            val s = ((other.x - x) * (other.x - x) + (other.y - y) * (other.y -y))
            val distance = sqrt(s)

            return "$distance"
        }
        return false
    }
}

class Fraction(n: Double , d: Double) {

    var numerator: Double = n
    var denominator: Double = d

    override fun equals(other: Any?): Boolean {
        if (other is Fraction) {
            if(numerator * other.denominator / denominator == other.numerator)

                return true

        }
        return false
    }

    override fun toString(): String {

        return "$numerator / $denominator"
    }

    fun reduction(): Any{

        var a = numerator
        var b = denominator

        while (a != b){
            if (a > b)
                a -= b

            else b -= a

        }
        numerator /= a
        denominator /= a
        return Point(numerator, denominator)
    }

    fun addition(other: Any?): Any {
        if (other is Fraction) {
            val numerator1 = (other.denominator * numerator) + (denominator * other.numerator)
            val denominator1 = denominator * other.denominator

            return Fraction(numerator1 , denominator1)
        }
        return false
    }

    fun subtraction(other: Any?): Any {
        if (other is Fraction){
            val numerator2 = (other.denominator * numerator) - (denominator * other.numerator)
            val denominator2 = (denominator * other.denominator)

            return Fraction(numerator2 , denominator2)
        }
        return false
    }

    fun multiply(other: Any?): Any {
        if (other is Fraction){

            val numerator3 = numerator * other.numerator
            val denominator3 = denominator * other.denominator

            return Fraction(numerator3 , denominator3)

        }
        return false
    }


    fun division(other: Any?): Any{
        if (other is Fraction){

            val numerator4 = numerator * other.denominator
            val denominator4 = denominator * other.numerator

            return Fraction(numerator4 , denominator4)
        }
        return false
    }
}